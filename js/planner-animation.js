(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"planner_animation_atlas_NP_", frames: [[0,802,1200,800],[0,1604,1200,800],[0,0,1200,800],[0,2406,1200,800]]}
];


// symbols:



(lib.PLATSAplanneranimation01 = function() {
	this.spriteSheet = ss["planner_animation_atlas_NP_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.PLATSAplanneranimation02 = function() {
	this.spriteSheet = ss["planner_animation_atlas_NP_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.PLATSAplanneranimation03 = function() {
	this.spriteSheet = ss["planner_animation_atlas_NP_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.PLATSAplanneranimation04 = function() {
	this.spriteSheet = ss["planner_animation_atlas_NP_"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.LetterUa = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAPgPAUAAQAVAAAPAPQAPAPAAAUQAAAVgPAPQgPAPgVAAQgUAAgPgPg");
	this.shape.setTransform(-35.2,35.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAPgPAUAAQAVAAAPAPQAPAPAAAUQAAAVgPAPQgPAPgVAAQgUAAgPgPg");
	this.shape_1.setTransform(-11.4,35.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAPgPAUAAQAVAAAPAPQAPAPAAAUQAAAVgPAPQgPAPgVAAQgUAAgPgPg");
	this.shape_2.setTransform(12.3,35.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#359ED5").s().p("AgjAkQgPgPABgVQgBgUAPgPQAPgPAUAAQAVAAAPAPQAOAPAAAUQAAAVgOAPQgPAPgVAAQgUAAgPgPg");
	this.shape_3.setTransform(35.3,35.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#359ED5").s().p("AgjAkQgPgPABgVQgBgUAPgOQAPgPAUgBQAVABAPAPQAOAOAAAUQAAAVgOAPQgPAOgVAAQgUAAgPgOg");
	this.shape_4.setTransform(35.3,-35.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgOQAPgPAUgBQAVABAPAPQAPAOAAAUQAAAVgPAPQgPAOgVAAQgUAAgPgOg");
	this.shape_5.setTransform(-35.2,-35.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#359ED5").p("AlflfIAAK/IDlAAIAAnvID1AAIAAHvIDlAAIAAq/g");
	this.shape_6.setTransform(0.1,0);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AB7FgIAAnvIj1AAIAAHvIjlAAIAAq/IK/AAIAAK/g");
	this.shape_7.setTransform(0.1,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.LetterUa, new cjs.Rectangle(-40.2,-40.2,80.6,80.5), null);


(lib.LetterU = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#359ED5").s().p("AgiAkQgPgPgBgVQABgUAPgPQAOgOAUAAQAVAAAPAOQAOAPAAAUQAAAVgOAPQgPAPgVAAQgUAAgOgPg");
	this.shape.setTransform(-12.2,14.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#359ED5").s().p("AgiAkQgPgPgBgVQABgUAPgOQAOgPAUgBQAVABAPAPQAOAOAAAUQAAAVgOAPQgPAOgVAAQgUAAgOgOg");
	this.shape_1.setTransform(-12.2,-35.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgOQAPgPAUgBQAVABAPAPQAPAOAAAUQAAAVgPAPQgPAOgVAAQgUAAgPgOg");
	this.shape_2.setTransform(12.4,-35.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAPgOAUAAQAVAAAPAOQAPAPAAAUQAAAVgPAPQgPAPgVAAQgUAAgPgPg");
	this.shape_3.setTransform(12.4,14.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#359ED5").s().p("AgjAkQgPgPABgVQgBgUAPgOQAPgPAUgBQAVABAPAPQAOAOAAAUQAAAVgOAPQgPAOgVAAQgUAAgPgOg");
	this.shape_4.setTransform(35.3,-35.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#359ED5").s().p("AgjAkQgPgPABgVQgBgUAPgPQAPgPAUAAQAVAAAPAPQAOAPAAAUQAAAVgOAPQgPAPgVAAQgUAAgPgPg");
	this.shape_5.setTransform(35.3,35.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAPgPAUAAQAVAAAPAPQAPAPAAAUQAAAVgPAPQgPAPgVAAQgUAAgPgPg");
	this.shape_6.setTransform(-35.2,35.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgOQAPgPAUgBQAVABAPAPQAPAOAAAUQAAAVgPAPQgPAOgVAAQgUAAgPgOg");
	this.shape_7.setTransform(-35.2,-35.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#359ED5").p("AB7lfIAAHvIj1AAIAAnvIjlAAIAAK/IK/AAIAAq/g");
	this.shape_8.setTransform(0.1,0);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AlfFgIAAq/IDlAAIAAHvID1AAIAAnvIDlAAIAAK/g");
	this.shape_9.setTransform(0.1,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.LetterU, new cjs.Rectangle(-40.2,-40.2,80.6,80.5), null);


(lib.LetterSq = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgOQAPgPAUgBQAVABAPAPQAPAOAAAUQAAAVgPAPQgPAOgVAAQgUAAgPgOg");
	this.shape.setTransform(-35.2,-35.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#359ED5").s().p("AgjAkQgPgPABgVQgBgUAPgOQAPgPAUgBQAVABAPAPQAOAOAAAUQAAAVgOAPQgPAOgVAAQgUAAgPgOg");
	this.shape_1.setTransform(35.3,-35.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#359ED5").s().p("AgjAkQgPgPABgVQgBgUAPgPQAPgPAUAAQAVAAAPAPQAOAPAAAUQAAAVgOAPQgPAPgVAAQgUAAgPgPg");
	this.shape_2.setTransform(35.3,35.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAPgPAUAAQAVAAAPAPQAPAPAAAUQAAAVgPAPQgPAPgVAAQgUAAgPgPg");
	this.shape_3.setTransform(-35.2,35.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#359ED5").p("AFglfIq/AAIAAK/IK/AAg");
	this.shape_4.setTransform(0.1,0);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AlfFfIAAq+IK/AAIAAK+g");
	this.shape_5.setTransform(0.1,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.LetterSq, new cjs.Rectangle(-40.2,-40.2,80.6,80.5), null);


(lib.LetterLc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgOQAPgPAUgBQAVABAPAPQAPAOAAAUQAAAVgPAPQgPAOgVAAQgUAAgPgOg");
	this.shape.setTransform(12.3,-14.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAPgOAUAAQAVAAAPAOQAPAPAAAUQAAAVgPAPQgPAPgVAAQgUAAgPgPg");
	this.shape_1.setTransform(12.3,35.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgOQAPgPAUgBQAVABAPAPQAPAOAAAUQAAAVgPAPQgPAOgVAAQgUAAgPgOg");
	this.shape_2.setTransform(-35.2,-14.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAPgPAUAAQAVAAAPAPQAPAPAAAUQAAAVgPAPQgPAPgVAAQgUAAgPgPg");
	this.shape_3.setTransform(-35.2,-35.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAPgPAUAAQAVAAAPAPQAPAPAAAUQAAAVgPAPQgPAPgVAAQgUAAgPgPg");
	this.shape_4.setTransform(35.2,-35.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAPgOAUAAQAVAAAPAOQAPAPAAAUQAAAVgPAPQgPAPgVAAQgUAAgPgPg");
	this.shape_5.setTransform(35.2,35.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#359ED5").p("AlfleIK/AAIAAK9IjlAAIAAnuInaAAg");
	this.shape_6.setTransform(0.1,0.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AB7FfIAAnvInaAAIAAjOIK/AAIAAK9g");
	this.shape_7.setTransform(0.1,0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.LetterLc, new cjs.Rectangle(-40.2,-40.1,80.6,80.4), null);


(lib.LetterL = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#359ED5").s().p("AgiAkQgPgPgBgVQABgUAPgPQAOgPAUAAQAVAAAPAPQAOAPAAAUQAAAVgOAPQgPAPgVAAQgUAAgOgPg");
	this.shape.setTransform(-12.2,14.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#359ED5").s().p("AgiAkQgPgPgBgVQABgUAPgOQAOgPAUgBQAVABAPAPQAOAOAAAUQAAAVgOAPQgPAOgVAAQgUAAgOgOg");
	this.shape_1.setTransform(-12.2,-35.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#359ED5").s().p("AgjAkQgPgPABgVQgBgUAPgPQAPgPAUAAQAVAAAPAPQAOAPAAAUQAAAVgOAPQgPAPgVAAQgUAAgPgPg");
	this.shape_2.setTransform(35.3,14.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#359ED5").s().p("AgjAkQgPgPABgVQgBgUAPgPQAPgOAUAAQAVAAAPAOQAOAPAAAUQAAAVgOAPQgPAPgVAAQgUAAgPgPg");
	this.shape_3.setTransform(35.3,35.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAPgOAUAAQAVAAAPAOQAPAPAAAUQAAAVgPAPQgPAPgVAAQgUAAgPgPg");
	this.shape_4.setTransform(-35.2,35.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgOQAPgPAUgBQAVABAPAPQAPAOAAAUQAAAVgPAPQgPAOgVAAQgUAAgPgOg");
	this.shape_5.setTransform(-35.2,-35.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#359ED5").p("AFgFfIq/AAIAAq9IDlAAIAAHuIHaAAg");
	this.shape_6.setTransform(0.1,0);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AlfFfIAAq9IDlAAIAAHuIHaAAIAADPg");
	this.shape_7.setTransform(0.1,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.LetterL, new cjs.Rectangle(-40.2,-40.1,80.6,80.4), null);


(lib.LetterA = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#359ED5").s().p("AgjAkQgOgPAAgVQAAgUAOgPQAPgPAUAAQAVAAAPAPQAPAPAAAUQAAAVgPAPQgPAPgVAAQgUAAgPgPg");
	this.shape.setTransform(14.5,-35.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#359ED5").s().p("AgiAkQgPgPgBgVQABgUAPgPQAOgPAUAAQAVAAAPAPQAOAPAAAUQAAAVgOAPQgPAPgVAAQgUAAgOgPg");
	this.shape_1.setTransform(-12.2,-35.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#359ED5").s().p("AgjAkQgPgPABgVQgBgUAPgPQAPgOAUAAQAVAAAPAOQAOAPAAAUQAAAVgOAPQgPAPgVAAQgUAAgPgPg");
	this.shape_2.setTransform(35.3,35.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAPgOAUAAQAVAAAPAOQAPAPAAAUQAAAVgPAPQgPAPgVAAQgUAAgPgPg");
	this.shape_3.setTransform(-35.2,35.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#359ED5").s().p("AgjAkQgPgPABgVQgBgUAPgOQAPgPAUgBQAVABAPAPQAOAOAAAUQAAAVgOAPQgPAOgVAAQgUAAgPgOg");
	this.shape_4.setTransform(35.3,14.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgOQAPgPAUgBQAVABAPAPQAPAOAAAUQAAAVgPAPQgPAOgVAAQgUAAgPgOg");
	this.shape_5.setTransform(-35.2,14.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#359ED5").p("AFgFgIq/AAIAAjQIDlnvIEKAAIDQHvg");
	this.shape_6.setTransform(0.1,0.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AlfFgIAAjQIDlnuIEKAAIDQHuIAADQg");
	this.shape_7.setTransform(0.1,0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.LetterA, new cjs.Rectangle(-40.2,-40.2,80.6,80.5), null);


(lib.Letter4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#359ED5").s().p("AgiAkQgPgPgBgVQABgUAPgOQAOgPAUgBQAVABAPAPQAOAOAAAUQAAAVgOAPQgPAOgVAAQgUAAgOgOg");
	this.shape.setTransform(-14.4,-35.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#359ED5").s().p("AgiAkQgPgPgBgVQABgUAPgOQAOgPAUgBQAVABAPAPQAOAOAAAUQAAAVgOAPQgPAOgVAAQgUAAgOgOg");
	this.shape_1.setTransform(-35.2,-35.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#359ED5").s().p("AgiAkQgPgPgBgVQABgUAPgPQAOgPAUAAQAVAAAPAPQAOAPAAAUQAAAVgOAPQgPAPgVAAQgUAAgOgPg");
	this.shape_2.setTransform(-35.2,35.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAPgPAUAAQAVAAAPAPQAPAPAAAUQAAAVgPAPQgPAPgVAAQgUAAgPgPg");
	this.shape_3.setTransform(35.2,35.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAPgOAUAAQAVAAAPAOQAPAPAAAUQAAAVgPAPQgPAPgVAAQgUAAgPgPg");
	this.shape_4.setTransform(35.2,14.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#359ED5").p("AlfFgIK/AAIAAjQInvnvIjQAAg");

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AlfFfIAAq+IDQAAIHvHvIAADPg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Letter4, new cjs.Rectangle(-40.2,-40.2,80.6,80.5), null);


(lib.LetterLb = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#359ED5").s().p("AgiAkQgPgPgBgVQABgUAPgOQAOgPAUgBQAVABAPAPQAOAOAAAUQAAAVgOAPQgPAOgVAAQgUAAgOgOg");
	this.shape.setTransform(-12.2,-14.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#359ED5").s().p("AgiAkQgPgPgBgVQABgUAPgPQAOgOAUAAQAVAAAPAOQAOAPAAAUQAAAVgOAPQgPAPgVAAQgUAAgOgPg");
	this.shape_1.setTransform(-12.2,35.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#359ED5").s().p("AgjAkQgPgPABgVQgBgUAPgOQAPgPAUgBQAVABAPAPQAOAOAAAUQAAAVgOAPQgPAOgVAAQgUAAgPgOg");
	this.shape_2.setTransform(35.3,-14.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#359ED5").s().p("AgjAkQgPgPABgVQgBgUAPgPQAPgPAUAAQAVAAAPAPQAOAPAAAUQAAAVgOAPQgPAPgVAAQgUAAgPgPg");
	this.shape_3.setTransform(35.3,-35.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAPgPAUAAQAVAAAPAPQAPAPAAAUQAAAVgPAPQgPAPgVAAQgUAAgPgPg");
	this.shape_4.setTransform(-35.2,-35.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAPgOAUAAQAVAAAPAOQAPAPAAAUQAAAVgPAPQgPAPgVAAQgUAAgPgPg");
	this.shape_5.setTransform(-35.2,35.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#359ED5").p("AFgleIq/AAIAAK9IDlAAIAAnuIHaAAg");
	this.shape_6.setTransform(0.1,0.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AlfFfIAAq9IK/AAIAADOInaAAIAAHvg");
	this.shape_7.setTransform(0.1,0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.LetterLb, new cjs.Rectangle(-40.2,-40.1,80.6,80.4), null);


(lib.LetterLa = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAPgPAUAAQAVAAAPAPQAPAPAAAUQAAAVgPAPQgPAPgVAAQgUAAgPgPg");
	this.shape.setTransform(12.3,14.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgOQAPgPAUgBQAVABAPAPQAPAOAAAUQAAAVgPAPQgPAOgVAAQgUAAgPgOg");
	this.shape_1.setTransform(12.3,-35.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAPgPAUAAQAVAAAPAPQAPAPAAAUQAAAVgPAPQgPAPgVAAQgUAAgPgPg");
	this.shape_2.setTransform(-35.2,14.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAPgOAUAAQAVAAAPAOQAPAPAAAUQAAAVgPAPQgPAPgVAAQgUAAgPgPg");
	this.shape_3.setTransform(-35.2,35.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAPgOAUAAQAVAAAPAOQAPAPAAAUQAAAVgPAPQgPAPgVAAQgUAAgPgPg");
	this.shape_4.setTransform(35.2,35.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#359ED5").s().p("AgjAkQgPgPAAgVQAAgUAPgOQAPgPAUgBQAVABAPAPQAPAOAAAUQAAAVgPAPQgPAOgVAAQgUAAgPgOg");
	this.shape_5.setTransform(35.2,-35.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#359ED5").p("AlfFfIK/AAIAAq9IjlAAIAAHuInaAAg");
	this.shape_6.setTransform(0.1,0);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AlfFfIAAjPIHaAAIAAnuIDlAAIAAK9g");
	this.shape_7.setTransform(0.1,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.LetterLa, new cjs.Rectangle(-40.2,-40.1,80.6,80.4), null);


(lib.IMG04 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// FlashAICB
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#359ED5").s().p("Ah7B8Qgzg0AAhIQAAhIAzgyQAzg0BIAAQBJAAAzA0QAzAyAABIQAABJgzAzQgzAzhJAAQhIAAgzgzg");
	this.shape.setTransform(-186.4,208.8,0.736,0.736);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#359ED5").s().p("Ah7B8Qgzg0AAhIQAAhIAzgyQA0g0BHAAQBJAAAyA0QA0AyAABIQAABIg0A0QgyAzhJAAQhHAAg0gzg");
	this.shape_1.setTransform(321.9,208.8,0.736,0.736);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#359ED5").s().p("Ah7B8QgzgzAAhJQAAhIAzgzQA0gzBHAAQBJAAAyAzQA0AzAABIQAABJg0AzQgyAzhJAAQhHAAg0gzg");
	this.shape_2.setTransform(321.9,31.3,0.736,0.736);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#359ED5").s().p("Ah7B8Qgzg0AAhIQAAhHAzg0QA0gzBHAAQBJAAAzAzQAzA0AABHQAABIgzA0QgzAzhJAAQhHAAg0gzg");
	this.shape_3.setTransform(-21.9,-261.7,0.736,0.736);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#359ED5").s().p("Ah7B8Qgzg0AAhIQAAhHAzg0QA0gzBHAAQBJAAAzAzQAzA0AABHQAABIgzA0QgzAzhJAAQhHAAg0gzg");
	this.shape_4.setTransform(-186.9,-261.7,0.736,0.736);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#359ED5").ss(7.5,1,1).p("Eg18gx9MAjGAAAMBIzA+VMAAAAlmMhr5AAAg");
	this.shape_5.setTransform(67.8,-26.6,0.736,0.736);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_1
	this.instance = new lib.PLATSAplanneranimation01();
	this.instance.parent = this;
	this.instance.setTransform(-435,-290,0.725,0.725);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.IMG04, new cjs.Rectangle(-435,-290,870,580), null);


(lib.IMG03 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// FlashAICB
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#359ED5").s().p("Ah7B8QgzgzAAhJQAAhHAzg0QA0gzBHAAQBIAAA0AzQAzA0AABHQAABJgzAzQg0AzhIAAQhHAAg0gzg");
	this.shape.setTransform(-183,199.6,0.734,0.734);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#359ED5").s().p("Ah7B8QgzgzAAhJQAAhHAzg0QAzgzBIAAQBIAAA0AzQAzA0AABHQAABJgzAzQg0AzhIAAQhIAAgzgzg");
	this.shape_1.setTransform(208.3,199.6,0.734,0.734);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#359ED5").s().p("Ah7B8QgzgzAAhJQAAhHAzg0QAzgzBIAAQBIAAA0AzQAzA0AABHQAABJgzAzQg0AzhIAAQhIAAgzgzg");
	this.shape_2.setTransform(208.3,-187.3,0.734,0.734);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#359ED5").s().p("Ah7B8QgzgzAAhJQAAhHAzg0QA0gzBHAAQBJAAAzAzQAzA0AABHQAABJgzAzQgzAzhJAAQhHAAg0gzg");
	this.shape_3.setTransform(-182.7,-187.3,0.734,0.734);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#359ED5").ss(6,1,1).p("EgplgpPMBTLAAAMAAABSfMhTLAAAg");
	this.shape_4.setTransform(12.7,6.1,0.734,0.734);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_1
	this.instance = new lib.PLATSAplanneranimation02();
	this.instance.parent = this;
	this.instance.setTransform(-435,-290,0.725,0.725);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.IMG03, new cjs.Rectangle(-435,-290,870,580), null);


(lib.IMG02 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// FlashAICB
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#359ED5").s().p("Ah7B7QgzgzAAhIQAAhHAzgzQA0g0BHAAQBIAAA0A0QAzAzAABHQAABIgzAzQg0A0hIAAQhHAAg0g0g");
	this.shape.setTransform(-187,191.7,0.73,0.73);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#359ED5").s().p("Ah7B7QgzgyAAhJQAAhHAzgzQA0g0BHAAQBJAAAzA0QAzAzAABHQAABJgzAyQgzA0hJAAQhHAAg0g0g");
	this.shape_1.setTransform(201.7,28.1,0.73,0.73);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#359ED5").s().p("Ah7B7QgzgzAAhIQAAhHAzgzQA0g0BHAAQBJAAAzA0QAzAzAABHQAABIgzAzQgzA0hJAAQhHAAg0g0g");
	this.shape_2.setTransform(201.7,191.7,0.73,0.73);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#359ED5").s().p("Ah7B7QgzgzAAhIQAAhHAzgzQA0g0BHAAQBIAAAzA0QA0AzAABHQAABIg0AzQgzA0hIAAQhHAAg0g0g");
	this.shape_3.setTransform(-17.1,27.4,0.73,0.73);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#359ED5").s().p("Ah6B8Qg0gzAAhJQAAhHA0g0QAzgzBHAAQBJAAAyAzQA0A0AABHQAABJg0AzQgyAzhJAAQhHAAgzgzg");
	this.shape_4.setTransform(-17.3,-194.1,0.73,0.73);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#359ED5").s().p("Ah7B8QgzgzAAhJQAAhHAzg0QA0gzBHAAQBJAAAzAzQAzA0AABHQAABJgzAzQgzAzhJAAQhHAAg0gzg");
	this.shape_5.setTransform(-186.7,-194.1,0.73,0.73);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#359ED5").ss(6,1,1).p("EgppgpPMAkVAAAMAAAAvUMAu+AAAMgAHAjLMhTMAAAg");
	this.shape_6.setTransform(7.7,-1.3,0.73,0.73);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_1
	this.instance = new lib.PLATSAplanneranimation03();
	this.instance.parent = this;
	this.instance.setTransform(-434.9,-290,0.725,0.725);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.IMG02, new cjs.Rectangle(-434.9,-290,870,580), null);


(lib.IMG01 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// FlashAICB
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#359ED5").s().p("Ah7B7QgzgzAAhIQAAhIAzgyQA0g0BHAAQBIAAA0A0QAzAyAABIQAABIgzAzQg0A0hIAAQhHAAg0g0g");
	this.shape.setTransform(-180.4,191.4,0.713,0.713);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#359ED5").s().p("Ah7B7QgzgzAAhIQAAhIAzgyQAzg0BIAAQBIAAA0A0QAzAyAABIQAABIgzAzQg0A0hIAAQhIAAgzg0g");
	this.shape_1.setTransform(199.9,191.4,0.713,0.713);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#359ED5").s().p("Ah7B8QgzgzAAhJQAAhHAzg0QAzgzBIAAQBIAAA0AzQAzA0AABHQAABJgzAzQg0AzhIAAQhIAAgzgzg");
	this.shape_2.setTransform(199.9,-184.6,0.713,0.713);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#359ED5").s().p("Ah7B7QgzgzAAhIQAAhHAzgzQAzg0BIAAQBIAAA0A0QAzAzAABHQAABIgzAzQg0A0hIAAQhIAAgzg0g");
	this.shape_3.setTransform(-23.1,31.7,0.713,0.713);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#359ED5").s().p("Ah6B7Qg0gzAAhIQAAhHA0gzQAyg0BIAAQBIAAA0A0QAzAzAABHQAABIgzAzQg0A0hIAAQhIAAgyg0g");
	this.shape_4.setTransform(46.3,31.7,0.713,0.713);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#359ED5").s().p("Ah6B8Qg0gzAAhJQAAhHA0g0QAygzBIAAQBIAAA0AzQAzA0AABHQAABJgzAzQg0AzhIAAQhIAAgygzg");
	this.shape_5.setTransform(46.3,-184.6,0.713,0.713);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#359ED5").s().p("Ah6B8Qg0gzAAhJQAAhHA0g0QAygzBIAAQBIAAAzAzQA0A0AABHQAABJg0AzQgzAzhIAAQhIAAgygzg");
	this.shape_6.setTransform(-22.6,-184.6,0.713,0.713);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#359ED5").s().p("Ah7B8QgzgzAAhJQAAhHAzg0QA0gzBHAAQBJAAAzAzQAzA0AABHQAABJgzAzQgzAzhJAAQhHAAg0gzg");
	this.shape_7.setTransform(-180.2,-184.6,0.713,0.713);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#359ED5").ss(6,1,1).p("EgplgpPMAidAAAMAAAAvUIPJAAMAAAgvUMAhlAAAMAAABSfMhTLAAAg");
	this.shape_8.setTransform(9.8,4.1,0.713,0.713);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_1
	this.instance = new lib.PLATSAplanneranimation04();
	this.instance.parent = this;
	this.instance.setTransform(-434.9,-290,0.725,0.725);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.IMG01, new cjs.Rectangle(-434.9,-290,870,580), null);


(lib.hand = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#1A1818").s().p("EAWiAroQjJgBk9AAIwtABQjvAAjYlFQh2iziRljQgdhLgIgNQjVllkfj/Qkej/mUjAQh/g9g+h3Qg3hpANhvQAMhjBHhPQBHhOB5gwQCNg3C2AAQClAAC9AvQDKAyCFBSIAAl6IAAgHMAAAgheQAAjECSiLQCRiLDNAAQBiAABbAjQBaAjBGBDQBHBDAmBXQAmBXAABgIAAN4QA+guBKgYQBNgaBSAAQCvAACHBrQCGBqAiCiQBBg0BPgcQBSgeBXAAQBhAABZAjQBYAjBFBBQA5A2AkBEQAjBDAMBLQBBgzBPgbQBQgcBVAAQBhAABZAjQBYAjBFBBQBGBDAlBVQAmBWAABeIAAYMQAACwhIEeQhJEeh0ETQh/EwiIC0QizDtiyAAgEAWjAogQBMAABwiQQB+ikB6kjQBpj6BGkPQBGkOAAiaIAA4MQAAg0gVgwQgWgxgoglQgpgng1gVQg1gVg6AAQg7AAg1AVQg1AVgpAnQgoAlgVAxQgVAwAAA0IAAJTQAAApgdAeQgeAdgpAAIgQAAQgpAAgegdQgdgeAAgpIAAt3QAAg1gVgwQgVgxgoglQgpgng1gVQg1gVg7AAQh3AAhUBPQhVBOAABvIAAN3QAAApgdAeQgdAdgqAAIgPAAQgpAAgegdQgdgeAAgpIAAyAQAAhvhVhOQhUhPh3AAQh3AAhVBPQhTBOAABvIAASAQAAApgeAeQgdAdgpAAIgIAAQgqAAgdgdQgdgeAAgpMAAAglsQAAg2gWgyQgWgxgpgnQgqgog3gVQg2gVg8AAQh7AAhWBQQhXBRAABxMAAAAiGIAAAHIAAIfQAAAegQAYQgPAZgbAMQgaAMgdgEQgdgDgXgTIhdhPQhYhKi7g2Qi7g2ipAAQiQAAhqApQg2AWglAcQg0AqgGAyQgGA0AcAzQAgA5BBAfQGvDOEzESQEzERDmGAQAMAVAnBeQBuERBgCjQCwErCWAAIQtgBQE+AADJABg");
	this.shape.setTransform(17.8,21.3,0.076,0.076);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("EAWiAroQjJgBk9AAIwtABQjvAAjYlFQh2iziRljQgdhLgIgNQjVllkfj/Qkej/mUjAQh/g9g+h3Qg3hpANhvQAMhjBHhPQBHhOB5gwQCNg3C2AAQClAAC9AvQDKAyCFBSIAAl6IAAgHMAAAgheQAAjECSiLQCRiLDNAAQBiAABbAjQBaAjBGBDQBHBDAmBXQAmBXAABgIAAN4QA+guBKgYQBNgaBSAAQCvAACHBrQCGBqAiCiQBBg0BPgcQBSgeBXAAQBhAABZAjQBYAjBFBBQA5A2AkBEQAjBDAMBLQBBgzBPgbQBQgcBVAAQBhAABZAjQBYAjBFBBQBGBDAlBVQAmBWAABeIAAYMQAACwhIEeQhJEeh0ETQh/EwiIC0QizDtiyAAg");
	this.shape_1.setTransform(17.8,21.3,0.076,0.076);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.hand, new cjs.Rectangle(0,0,35.5,42.7), null);


// stage content:
(lib.planneranimation = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// hand
	this.instance = new lib.hand();
	this.instance.parent = this;
	this.instance.setTransform(1113.9,115.5,0.781,0.781,0,0,0,17.8,21.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(20).to({y:114.5},0).to({x:1003.9,y:113.5},10).wait(44).to({y:326.5},10).wait(44).to({x:1113.9,y:220.5},11).wait(44).to({y:115.5},10).wait(23));

	// Letter A
	this.instance_1 = new lib.LetterA();
	this.instance_1.parent = this;
	this.instance_1.setTransform(989.3,504.8);
	this.instance_1.alpha = 0.699;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(216));

	// letter Lc
	this.instance_2 = new lib.LetterLc();
	this.instance_2.parent = this;
	this.instance_2.setTransform(1098,394.9);
	this.instance_2.alpha = 0.699;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(216));

	// letter Lb
	this.instance_3 = new lib.LetterLb();
	this.instance_3.parent = this;
	this.instance_3.setTransform(989.3,394.9);
	this.instance_3.alpha = 0.699;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(216));

	// letter La
	this.instance_4 = new lib.LetterLa();
	this.instance_4.parent = this;
	this.instance_4.setTransform(1098,289.2);
	this.instance_4.alpha = 0.699;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(216));

	// letter L
	this.instance_5 = new lib.LetterL();
	this.instance_5.parent = this;
	this.instance_5.setTransform(989.3,289.2);
	this.instance_5.alpha = 0.699;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(84).to({alpha:1},0).wait(17).to({scaleX:0.83,scaleY:0.83,y:289.1},0).wait(5).to({scaleX:1,scaleY:1,y:289.2},0).wait(55).to({alpha:0.699},0).wait(55));

	// letter u
	this.instance_6 = new lib.LetterU();
	this.instance_6.parent = this;
	this.instance_6.setTransform(1098,181.9);
	this.instance_6.alpha = 0.699;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(139).to({alpha:1},0).wait(17).to({scaleX:0.83,scaleY:0.83},0).wait(5).to({scaleX:1,scaleY:1},0).wait(54).to({alpha:0.699},0).wait(1));

	// letter ua
	this.instance_7 = new lib.LetterUa();
	this.instance_7.parent = this;
	this.instance_7.setTransform(989.3,181.9);
	this.instance_7.alpha = 0.699;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(77).to({alpha:1},0).wait(4).to({alpha:0.699},0).wait(135));

	// letter sq
	this.instance_8 = new lib.LetterSq();
	this.instance_8.parent = this;
	this.instance_8.setTransform(989.3,75.6);
	this.instance_8.alpha = 0.719;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(30).to({alpha:1},0).wait(17).to({scaleX:0.83,scaleY:0.83},0).wait(5).to({scaleX:1,scaleY:1},0).wait(54).to({alpha:0.699},0).wait(110));

	// letter 4
	this.instance_9 = new lib.Letter4();
	this.instance_9.parent = this;
	this.instance_9.setTransform(1098,75.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(52).to({alpha:0.691},0).wait(141).to({alpha:1},0).wait(17).to({scaleX:0.83,scaleY:0.83},0).wait(5).to({scaleX:1,scaleY:1},0).wait(1));

	// Layer_5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#C3A06E").s().p("EgYgAtUMAAAhanMAxBAAAMAAABang");
	this.shape.setTransform(1042.8,290);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(216));

	// IMG01
	this.instance_10 = new lib.IMG04();
	this.instance_10.parent = this;
	this.instance_10.setTransform(435,290);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(49).to({alpha:0},6).wait(155).to({alpha:1},5).wait(1));

	// IMG02
	this.instance_11 = new lib.IMG03();
	this.instance_11.parent = this;
	this.instance_11.setTransform(435,290);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(49).to({_off:false},0).wait(54).to({alpha:0},6).wait(107));

	// IMG03
	this.instance_12 = new lib.IMG02();
	this.instance_12.parent = this;
	this.instance_12.setTransform(435,290);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(103).to({_off:false},0).wait(55).to({alpha:0},6).wait(52));

	// IMG04
	this.instance_13 = new lib.IMG01();
	this.instance_13.parent = this;
	this.instance_13.setTransform(435,290);
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(158).to({_off:false},0).wait(58));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(600,290,1199.7,580);
// library properties:
lib.properties = {
	id: '0D82343838F341E78C4106A24DE1B948',
	width: 1200,
	height: 580,
	fps: 24,
	color: "#2A1F21",
	opacity: 1.00,
	manifest: [
		{src:"images/planner_animation_atlas_NP_.jpg", id:"planner_animation_atlas_NP_"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['0D82343838F341E78C4106A24DE1B948'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;