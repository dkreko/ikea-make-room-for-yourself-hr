var canvasA, stageA, exportRootA, anim_containerA, dom_overlay_containerA, fnStartAnimationA;
var animWidthA = $('.animationHolder').width();
function initA() {
	canvasA = document.getElementById("canvasA");
	anim_containerA = document.getElementById("animation_containerA");
	dom_overlay_containerA = document.getElementById("dom_overlay_containerA");
	var compA=AdobeAn.getComposition("F2CFE711181443C6BAEEECFCFE8F1088");
	var lib=compA.getLibrary();
	var loader = new createjs.LoadQueue(false);
	loader.addEventListener("fileload", function(evt){handleFileLoadA(evt,compA)});
	loader.addEventListener("complete", function(evt){handleCompleteA(evt,compA)});
	var lib=compA.getLibrary();
	loader.loadManifest(lib.properties.manifest);
}
function handleFileLoadA(evt, compA) {
	var images=compA.getImages();
	if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
}
function handleCompleteA(evt,compA) {
	//This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
	var lib=compA.getLibrary();
	var ss=compA.getSpriteSheet();
	var queue = evt.target;
	var ssMetadata = lib.ssMetadata;
	for(i=0; i<ssMetadata.length; i++) {
		ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
	}
	exportRootA = new lib.platsaInteriorotvaranje12();
	stageA = new lib.Stage(canvasA);
	stageA.enableMouseOver();
	//Registers the "tick" event listener.
	fnStartAnimationA = function() {
		stageA.addChild(exportRootA);
		createjs.Ticker.setFPS(lib.properties.fps);
		createjs.Ticker.addEventListener("tick", stageA);
	}
	//Code to support hidpi screens and responsive scaling.
	function makeResponsive(isResp, respDim, isScale, scaleType) {
		var lastW, lastH, lastS=1;
		window.addEventListener('resize', resizeCanvasA);
		resizeCanvasA();
		function resizeCanvasA() {
			var w = lib.properties.width, h = lib.properties.height;
			//var iw = window.innerWidth, ih=window.innerHeight;
			var iw = animWidthA, ih= animWidthA * 1.083;
			var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
			if(isResp) {
				if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
					sRatio = lastS;
				}
				else if(!isScale) {
					if(iw<w || ih<h)
						sRatio = Math.min(xRatio, yRatio);
				}
				else if(scaleType==1) {
					sRatio = Math.min(xRatio, yRatio);
				}
				else if(scaleType==2) {
					sRatio = Math.max(xRatio, yRatio);
				}
			}
			canvasA.width = w*pRatio*sRatio;
			canvasA.height = h*pRatio*sRatio;
			canvasA.style.width = dom_overlay_containerA.style.width = anim_containerA.style.width =  w*sRatio+'px';
			canvasA.style.height = anim_containerA.style.height = dom_overlay_containerA.style.height = h*sRatio+'px';
			stageA.scaleX = pRatio*sRatio;
			stageA.scaleY = pRatio*sRatio;
			lastW = iw; lastH = ih; lastS = sRatio;
			stageA.tickOnUpdate = false;
			stageA.update();
			stageA.tickOnUpdate = true;
		}
	}
	makeResponsive(true,'both',false,1);
	AdobeAn.compositionLoaded(lib.properties.id);
	fnStartAnimationA();
}