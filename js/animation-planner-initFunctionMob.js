var canvas2, stage2, exportRoot2, anim_container2, dom_overlay_container2, fnStartAnimation2;
var animWidth2 = $('.plannerAnimationHolder').width();
function init2() {
	canvas2 = document.getElementById("canvas2");
	anim_container2 = document.getElementById("animation_container2");
	dom_overlay_container2 = document.getElementById("dom_overlay_container2");
	var comp=AdobeAn.getComposition("0D82343838F341E78C4106A24DE1B948");
	var lib=comp.getLibrary();
	var loader = new createjs.LoadQueue(false);
	loader.addEventListener("fileload", function(evt){handleFileLoad2(evt,comp)});
	loader.addEventListener("complete", function(evt){handleComplete2(evt,comp)});
	var lib=comp.getLibrary();
	loader.loadManifest(lib.properties.manifest);
}
function handleFileLoad2(evt, comp) {
	var images=comp.getImages();	
	if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }	
}
function handleComplete2(evt,comp) {
	//This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
	var lib=comp.getLibrary();
	var ss=comp.getSpriteSheet();
	var queue = evt.target;
	var ssMetadata = lib.ssMetadata;
	for(i=0; i<ssMetadata.length; i++) {
		ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
	}
	exportRoot2 = new lib.planneranimationmob();
	stage2 = new lib.Stage(canvas2);	
	//Registers the "tick" event listener.
	fnStartAnimation2 = function() {
		stage2.addChild(exportRoot2);
		createjs.Ticker.setFPS(lib.properties.fps);
		createjs.Ticker.addEventListener("tick", stage2);
	}	    
	//Code to support hidpi screens and responsive scaling.
	function makeResponsive(isResp, respDim, isScale, scaleType) {		
		var lastW, lastH, lastS=1;		
		window.addEventListener('resize', resizeCanvas2);		
		resizeCanvas2();		
		function resizeCanvas2() {			
			var w = lib.properties.width, h = lib.properties.height;			
			var iw = animWidth2, ih= animWidth2 * 0.48;		
			var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
			if(isResp) {                
				if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
					sRatio = lastS;                
				}				
				else if(!isScale) {					
					if(iw<w || ih<h)						
						sRatio = Math.min(xRatio, yRatio);				
				}				
				else if(scaleType==1) {					
					sRatio = Math.min(xRatio, yRatio);				
				}				
				else if(scaleType==2) {					
					sRatio = Math.max(xRatio, yRatio);				
				}			
			}			
			canvas2.width = w*pRatio*sRatio;			
			canvas2.height = h*pRatio*sRatio;
			canvas2.style.width = dom_overlay_container2.style.width = anim_container2.style.width =  w*sRatio+'px';				
			canvas2.style.height = anim_container2.style.height = dom_overlay_container2.style.height = h*sRatio+'px';
			stage2.scaleX = pRatio*sRatio;			
			stage2.scaleY = pRatio*sRatio;			
			lastW = iw; lastH = ih; lastS = sRatio;            
			stage2.tickOnUpdate = false;            
			stage2.update();            
			stage2.tickOnUpdate = true;		
		}
	}
	makeResponsive(true,'both',false,1);	
	AdobeAn.compositionLoaded(lib.properties.id);
	fnStartAnimation2();
}