(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"platsa Interior otvaranje 1.1_atlas_P_", frames: [[509,600,250,219],[254,0,253,778],[0,0,252,821],[254,780,245,197],[509,295,245,303],[509,0,257,293]]}
];


// symbols:



(lib.Layer1 = function() {
	this.spriteSheet = ss["platsa Interior otvaranje 1.1_atlas_P_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.Layer2 = function() {
	this.spriteSheet = ss["platsa Interior otvaranje 1.1_atlas_P_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.Layer3 = function() {
	this.spriteSheet = ss["platsa Interior otvaranje 1.1_atlas_P_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.Layer4 = function() {
	this.spriteSheet = ss["platsa Interior otvaranje 1.1_atlas_P_"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.Layer5 = function() {
	this.spriteSheet = ss["platsa Interior otvaranje 1.1_atlas_P_"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.Layer6 = function() {
	this.spriteSheet = ss["platsa Interior otvaranje 1.1_atlas_P_"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.ZATVORENO = function() {
	this.initialize(img.ZATVORENO);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1920,1280);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#1A1818").s().p("EAWiAroQjJgBk9AAIwtABQjvAAjYlFQh2iziRljQgdhLgIgNQjVllkfj/Qkej/mUjAQh/g9g+h3Qg3hpANhvQAMhjBHhPQBHhOB5gwQCNg3C2AAQClAAC9AvQDKAyCFBSIAAl6IAAgHMAAAgheQAAjECSiLQCRiLDNAAQBiAABbAjQBaAjBGBDQBHBDAmBXQAmBXAABgIAAN4QA+guBKgYQBNgaBSAAQCvAACHBrQCGBqAiCiQBBg0BPgcQBSgeBXAAQBhAABZAjQBYAjBFBBQA5A2AkBEQAjBDAMBLQBBgzBPgbQBQgcBVAAQBhAABZAjQBYAjBFBBQBGBDAlBVQAmBWAABeIAAYMQAACwhIEeQhJEeh0ETQh/EwiIC0QizDtiyAAgEAWjAogQBMAABwiQQB+ikB6kjQBpj6BGkPQBGkOAAiaIAA4MQAAg0gVgwQgWgxgoglQgpgng1gVQg1gVg6AAQg7AAg1AVQg1AVgpAnQgoAlgVAxQgVAwAAA0IAAJTQAAApgdAeQgeAdgpAAIgQAAQgpAAgegdQgdgeAAgpIAAt3QAAg1gVgwQgVgxgoglQgpgng1gVQg1gVg7AAQh3AAhUBPQhVBOAABvIAAN3QAAApgdAeQgdAdgqAAIgPAAQgpAAgegdQgdgeAAgpIAAyAQAAhvhVhOQhUhPh3AAQh3AAhVBPQhTBOAABvIAASAQAAApgeAeQgdAdgpAAIgIAAQgqAAgdgdQgdgeAAgpMAAAglsQAAg2gWgyQgWgxgpgnQgqgog3gVQg2gVg8AAQh7AAhWBQQhXBRAABxMAAAAiGIAAAHIAAIfQAAAegQAYQgPAZgbAMQgaAMgdgEQgdgDgXgTIhdhPQhYhKi7g2Qi7g2ipAAQiQAAhqApQg2AWglAcQg0AqgGAyQgGA0AcAzQAgA5BBAfQGvDOEzESQEzERDmGAQAMAVAnBeQBuERBgCjQCwErCWAAIQtgBQE+AADJABg");
	this.shape.setTransform(0,0,0.076,0.076);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("EAWiAroQjJgBk9AAIwtABQjvAAjYlFQh2iziRljQgdhLgIgNQjVllkfj/Qkej/mUjAQh/g9g+h3Qg3hpANhvQAMhjBHhPQBHhOB5gwQCNg3C2AAQClAAC9AvQDKAyCFBSIAAl6IAAgHMAAAgheQAAjECSiLQCRiLDNAAQBiAABbAjQBaAjBGBDQBHBDAmBXQAmBXAABgIAAN4QA+guBKgYQBNgaBSAAQCvAACHBrQCGBqAiCiQBBg0BPgcQBSgeBXAAQBhAABZAjQBYAjBFBBQA5A2AkBEQAjBDAMBLQBBgzBPgbQBQgcBVAAQBhAABZAjQBYAjBFBBQBGBDAlBVQAmBWAABeIAAYMQAACwhIEeQhJEeh0ETQh/EwiIC0QizDtiyAAg");
	this.shape_1.setTransform(0,0,0.076,0.076);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-17.7,-21.3,35.5,42.7);


(lib.ZATVORENO_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.ZATVORENO();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.ZATVORENO_1, new cjs.Rectangle(0,0,1920,1280), null);


(lib.Layer6_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Layer6();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Layer6_1, new cjs.Rectangle(0,0,257,293), null);


(lib.Layer5_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Layer5();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Layer5_1, new cjs.Rectangle(0,0,245,303), null);


(lib.Layer4_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Layer4();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Layer4_1, new cjs.Rectangle(0,0,245,197), null);


(lib.Layer3_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Layer3();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Layer3_1, new cjs.Rectangle(0,0,252,821), null);


(lib.Layer2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Layer2();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Layer2_1, new cjs.Rectangle(0,0,253,778), null);


(lib.Layer1_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Layer1();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Layer1_1, new cjs.Rectangle(0,0,250,219), null);


(lib.circle = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#666666").ss(1,1,1).p("AA6AAQAAAYgRARQgRARgYAAQgXAAgRgRQgRgRAAgYQAAgXARgRQARgRAXAAQAYAAARARQARARAAAXg");
	this.shape.setTransform(5.8,5.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.circle, new cjs.Rectangle(-1,-1,13.6,13.6), null);


(lib.ButtonBigLeft = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("EgTrA6dMAAAh05MAnXAAAMAAAB05g");
	this.shape.setTransform(126,374.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.ButtonBigLeft, new cjs.Rectangle(0,0,252.1,748.2), null);


(lib.hotspotcopy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.circle();
	this.instance.parent = this;
	this.instance.setTransform(5.7,5.7,1,1,0,0,0,5.8,5.8);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(80).to({_off:false},0).to({regX:5.6,regY:5.6,scaleX:4.36,scaleY:4.36,x:4.9,y:4.9,alpha:0},25).wait(7));

	// Layer_3
	this.instance_1 = new lib.circle();
	this.instance_1.parent = this;
	this.instance_1.setTransform(5.7,5.7,1,1,0,0,0,5.8,5.8);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(73).to({_off:false},0).to({regX:5.6,regY:5.6,scaleX:4.36,scaleY:4.36,x:4.9,y:4.9,alpha:0},25).wait(14));

	// Layer_2
	this.instance_2 = new lib.circle();
	this.instance_2.parent = this;
	this.instance_2.setTransform(5.7,5.7,1,1,0,0,0,5.8,5.8);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(67).to({_off:false},0).to({regX:5.6,regY:5.6,scaleX:4.36,scaleY:4.36,x:4.9,y:4.9,alpha:0},25).wait(20));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#879FB5").s().p("AgoApQgQgRgBgYQABgXAQgRQARgQAXgBQAYABARAQQAQARABAXQgBAYgQARQgRAQgYABQgXgBgRgQg");
	this.shape.setTransform(5.8,5.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(112));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,11.5,11.5);


(lib.hotspotcopy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.circle();
	this.instance.parent = this;
	this.instance.setTransform(5.7,5.7,1,1,0,0,0,5.8,5.8);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(74).to({_off:false},0).to({regX:5.6,regY:5.6,scaleX:4.36,scaleY:4.36,x:4.9,y:4.9,alpha:0},25).wait(7));

	// Layer_3
	this.instance_1 = new lib.circle();
	this.instance_1.parent = this;
	this.instance_1.setTransform(5.7,5.7,1,1,0,0,0,5.8,5.8);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(67).to({_off:false},0).to({regX:5.6,regY:5.6,scaleX:4.36,scaleY:4.36,x:4.9,y:4.9,alpha:0},25).wait(14));

	// Layer_2
	this.instance_2 = new lib.circle();
	this.instance_2.parent = this;
	this.instance_2.setTransform(5.7,5.7,1,1,0,0,0,5.8,5.8);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(61).to({_off:false},0).to({regX:5.6,regY:5.6,scaleX:4.36,scaleY:4.36,x:4.9,y:4.9,alpha:0},25).wait(20));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#879FB5").s().p("AgoApQgQgRgBgYQABgXAQgRQARgQAXgBQAYABARAQQAQARABAXQgBAYgQARQgRAQgYABQgXgBgRgQg");
	this.shape.setTransform(5.8,5.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(106));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,11.5,11.5);


(lib.hotspotcopy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.circle();
	this.instance.parent = this;
	this.instance.setTransform(5.7,5.7,1,1,0,0,0,5.8,5.8);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(27).to({_off:false},0).to({regX:5.6,regY:5.6,scaleX:4.36,scaleY:4.36,x:4.9,y:4.9,alpha:0},25).to({_off:true},6).wait(50));

	// Layer_3
	this.instance_1 = new lib.circle();
	this.instance_1.parent = this;
	this.instance_1.setTransform(5.7,5.7,1,1,0,0,0,5.8,5.8);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(20).to({_off:false},0).to({regX:5.6,regY:5.6,scaleX:4.36,scaleY:4.36,x:4.9,y:4.9,alpha:0},25).to({_off:true},13).wait(50));

	// Layer_2
	this.instance_2 = new lib.circle();
	this.instance_2.parent = this;
	this.instance_2.setTransform(5.7,5.7,1,1,0,0,0,5.8,5.8);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(14).to({_off:false},0).to({regX:5.6,regY:5.6,scaleX:4.36,scaleY:4.36,x:4.9,y:4.9,alpha:0},25).to({_off:true},19).wait(50));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#879FB5").s().p("AgoApQgQgRgBgYQABgXAQgRQARgQAXgBQAYABARAQQAQARABAXQgBAYgQARQgRAQgYABQgXgBgRgQg");
	this.shape.setTransform(5.8,5.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(108));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,11.5,11.5);


(lib.hotspotcopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.circle();
	this.instance.parent = this;
	this.instance.setTransform(5.7,5.7,1,1,0,0,0,5.8,5.8);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(25).to({_off:false},0).to({regX:5.6,regY:5.6,scaleX:4.36,scaleY:4.36,x:4.9,y:4.9,alpha:0},25).to({_off:true},3).wait(13));

	// Layer_3
	this.instance_1 = new lib.circle();
	this.instance_1.parent = this;
	this.instance_1.setTransform(5.7,5.7,1,1,0,0,0,5.8,5.8);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(18).to({_off:false},0).to({regX:5.6,regY:5.6,scaleX:4.36,scaleY:4.36,x:4.9,y:4.9,alpha:0},25).to({_off:true},10).wait(13));

	// Layer_2
	this.instance_2 = new lib.circle();
	this.instance_2.parent = this;
	this.instance_2.setTransform(5.7,5.7,1,1,0,0,0,5.8,5.8);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(12).to({_off:false},0).to({regX:5.6,regY:5.6,scaleX:4.36,scaleY:4.36,x:4.9,y:4.9,alpha:0},25).to({_off:true},16).wait(13));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#879FB5").s().p("AgoApQgQgRgBgYQABgXAQgRQARgQAXgBQAYABARAQQAQARABAXQgBAYgQARQgRAQgYABQgXgBgRgQg");
	this.shape.setTransform(5.8,5.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(66));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,11.5,11.5);


(lib.hotspot = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.circle();
	this.instance.parent = this;
	this.instance.setTransform(5.7,5.7,1,1,0,0,0,5.8,5.8);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(13).to({_off:false},0).to({regX:5.6,regY:5.6,scaleX:4.36,scaleY:4.36,x:4.9,y:4.9,alpha:0},25).to({_off:true},6).wait(22));

	// Layer_3
	this.instance_1 = new lib.circle();
	this.instance_1.parent = this;
	this.instance_1.setTransform(5.7,5.7,1,1,0,0,0,5.8,5.8);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(6).to({_off:false},0).to({regX:5.6,regY:5.6,scaleX:4.36,scaleY:4.36,x:4.9,y:4.9,alpha:0},25).to({_off:true},6).wait(29));

	// Layer_2
	this.instance_2 = new lib.circle();
	this.instance_2.parent = this;
	this.instance_2.setTransform(5.7,5.7,1,1,0,0,0,5.8,5.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:5.6,regY:5.6,scaleX:4.36,scaleY:4.36,x:4.9,y:4.9,alpha:0},25).to({_off:true},1).wait(40));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#879FB5").s().p("AgoApQgQgRgBgYQABgXAQgRQARgQAXgBQAYABARAQQAQARABAXQgBAYgQARQgRAQgYABQgXgBgRgQg");
	this.shape.setTransform(5.8,5.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(66));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.6,-0.6,12.6,12.6);


(lib.hand = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_10 = new cjs.Graphics().p("AAAAoQgPAAgNgMQgLgMAAgQQAAgPALgNIAAAAQANgLAPAAQAQAAAMALQAMANAAAPQAAAQgMAMIAAAAQgMAMgQAAIAAAAgAAAgYQgJAAgIAHIAAAAQgHAIAAAJQAAAKAHAHQAIAIAJAAQAKAAAHgIIAAAAQAIgHAAgKQAAgJgIgIQgHgHgKAAIAAAAg");
	var mask_graphics_11 = new cjs.Graphics().p("Ag9A+QgagaAAgkQgBgjAagaIABgBQAagaAjABQAkAAAaAZQAaAbAAAjQAAAjgaAaIgBABQgaAagjAAQgjAAgagagAgighQgNAPAAASIAAAAQAAASANAOIACADQAOANASAAIAAAAQASAAAPgOIABgCQAOgNAAgTQAAgSgPgQQgOgNgTAAQgRAAgRAOg");
	var mask_graphics_12 = new cjs.Graphics().p("AhfBfQgogoAAg3QAAg2AngpIABgBQApgnA2AAQA3AAAoAoQAqApgBA2QAAA3goAoIgBABQgoAog3AAIgBAAQg2AAgogpgAgygwQgUAVAAAbIAAAAQAAAcAUAVIAEADQAUATAaAAIABAAQAbgBAVgUIACgDQAUgUAAgbQAAgcgWgWQgVgUgcAAQgaAAgYAWg");
	var mask_graphics_13 = new cjs.Graphics().p("AiBCBQg2g2AAhLQgBhJA2g4IABgCQA4g1BJABQBLAAA2A1QA4A4gBBKQAABKg3A2IgBABQg2A3hKAAIgCAAQhIAAg3g3gAhDhAQgaAcAAAkIAAAAQAAAlAbAbIAFAFQAaAYAjABIABAAQAkgBAbgbIAEgEQAagaAAgkQgBglgdgeQgcgagkAAQgkAAgfAdg");
	var mask_graphics_14 = new cjs.Graphics().p("AijCiQhEhEgBhfQAAhcBEhGIABgBQBGhEBdABQBfAABDBEQBHBGAABcQAABehGBEIgCADQhDBFhegBQhdABhGhHgAgBh0QgtAAgmAkQggAjAAAsIAAABQAAAvAiAhIAGAGQAhAeArABIABAAQAugBAhgjIAFgEQAgghAAgtQgBgtglglQghghguAAIgBAAg");
	var mask_graphics_15 = new cjs.Graphics().p("AjFDEQhShSAAhzQgBhvBRhVIACgCQBVhRBwABQByAABSBRQBVBWAABvQAAByhUBSIgCACQhSBUhxAAQhwAAhVhVgAgBiLQg2AAguArQgmAqAAA1IAAABQAAA4AoAoIAIAHQAoAkAzABIACAAQA2gCAngoIAGgGQAmgnABg2QgBg3gtgsQgngng3AAIgBAAg");
	var mask_graphics_16 = new cjs.Graphics().p("AjnDlQhhhgAAiGQAAiCBghkIABgCQBjhfCEAAQCGAABfBgQBkBkgBCDQAACFhhBgIgDADQhfBhiFAAIgCAAQiDABhihkgAgBiiQg/AAg1AyQgtAxABA+IAAABQAABCAvAuIAIAIQAuApA8ACIACAAQA/gDAuguIAHgHQAtguAAg+QgChBgzgzQgugthAAAIgBAAg");
	var mask_graphics_17 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");
	var mask_graphics_18 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");
	var mask_graphics_19 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");
	var mask_graphics_20 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");
	var mask_graphics_21 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");
	var mask_graphics_22 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");
	var mask_graphics_23 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");
	var mask_graphics_24 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");
	var mask_graphics_25 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");
	var mask_graphics_26 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");
	var mask_graphics_27 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");
	var mask_graphics_28 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");
	var mask_graphics_29 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");
	var mask_graphics_30 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");
	var mask_graphics_31 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");
	var mask_graphics_32 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");
	var mask_graphics_33 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");
	var mask_graphics_34 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");
	var mask_graphics_35 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");
	var mask_graphics_36 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");
	var mask_graphics_37 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");
	var mask_graphics_38 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");
	var mask_graphics_39 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");
	var mask_graphics_40 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");
	var mask_graphics_41 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");
	var mask_graphics_42 = new cjs.Graphics().p("AkJEHQhuhuAAiaQgCiWBuhxIADgDQBxhtCWABQCaAABuBtQByBzAACWQAACZhwBuIgEADQhtBwiZAAIgBAAQiWAAhxhygAgBi5QhIAAg4A1IgCABIgBACQg0A4AABIQgBBLA2A2QA4A4BKACQBKgBA1g3IAEgDQA3g2ABhKQgChKg4g4Qg2g2hKAAIgBAAg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(10).to({graphics:mask_graphics_10,x:14.1,y:1.6}).wait(1).to({graphics:mask_graphics_11,x:14.1,y:1.6}).wait(1).to({graphics:mask_graphics_12,x:14.1,y:1.6}).wait(1).to({graphics:mask_graphics_13,x:14.2,y:1.7}).wait(1).to({graphics:mask_graphics_14,x:14.2,y:1.7}).wait(1).to({graphics:mask_graphics_15,x:14.2,y:1.7}).wait(1).to({graphics:mask_graphics_16,x:14.2,y:1.7}).wait(1).to({graphics:mask_graphics_17,x:14.3,y:1.8}).wait(1).to({graphics:mask_graphics_18,x:14.3,y:1.8}).wait(1).to({graphics:mask_graphics_19,x:14.3,y:1.8}).wait(1).to({graphics:mask_graphics_20,x:14.3,y:1.8}).wait(1).to({graphics:mask_graphics_21,x:14.3,y:1.8}).wait(1).to({graphics:mask_graphics_22,x:14.3,y:1.8}).wait(1).to({graphics:mask_graphics_23,x:14.3,y:1.8}).wait(1).to({graphics:mask_graphics_24,x:14.3,y:1.8}).wait(1).to({graphics:mask_graphics_25,x:14.3,y:1.8}).wait(1).to({graphics:mask_graphics_26,x:14.3,y:1.8}).wait(1).to({graphics:mask_graphics_27,x:14.3,y:1.8}).wait(1).to({graphics:mask_graphics_28,x:14.3,y:1.8}).wait(1).to({graphics:mask_graphics_29,x:14.3,y:1.8}).wait(1).to({graphics:mask_graphics_30,x:14.3,y:1.8}).wait(1).to({graphics:mask_graphics_31,x:14.3,y:1.8}).wait(1).to({graphics:mask_graphics_32,x:14.3,y:1.8}).wait(1).to({graphics:mask_graphics_33,x:14.3,y:1.8}).wait(1).to({graphics:mask_graphics_34,x:14.3,y:1.8}).wait(1).to({graphics:mask_graphics_35,x:14.3,y:1.8}).wait(1).to({graphics:mask_graphics_36,x:14.3,y:1.8}).wait(1).to({graphics:mask_graphics_37,x:14.3,y:1.8}).wait(1).to({graphics:mask_graphics_38,x:14.3,y:1.8}).wait(1).to({graphics:mask_graphics_39,x:14.3,y:1.8}).wait(1).to({graphics:mask_graphics_40,x:14.3,y:1.8}).wait(1).to({graphics:mask_graphics_41,x:14.3,y:1.8}).wait(1).to({graphics:mask_graphics_42,x:14.3,y:1.8}).wait(1));

	// click
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1.5,1,1).p("AByggIhMBLAhxggIBMBMAAAhHIAABrACjBIIhsAAAg3BIIhrAA");
	this.shape.setTransform(13.9,-7.1);
	this.shape._off = true;

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(10).to({_off:false},0).wait(33));

	// TEXT
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAaA+Ig0hUIAABUIgdAAIAAh7IAmAAIAsBGIAAhGIAdAAIAAB7g");
	this.shape_1.setTransform(86.7,77.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgsA+IAAh7IBYAAIAAAYIg4AAIAAAVIA1AAIAAAXIg1AAIAAAfIA4AAIAAAYg");
	this.shape_2.setTransform(74,77.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgyA+IAAh7IAzAAQAMAAAIACQAIACAGAEQAIAFAEAIQAEAHAAALQAAAIgDAIQgDAHgGAGQgHAHgJAEQgJADgOAAIgSAAIAAApgAgSgBIAJAAIAMgBQAFgBAEgDQADgCACgEQABgEAAgFQAAgFgDgEQgDgEgDgBIgJgDIgNAAIgFAAg");
	this.shape_3.setTransform(62.2,77.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AguAvQgRgRAAgeQAAgdARgRQARgRAdAAQAeAAARARQARARAAAdQAAAegRARQgRARgeAAQgdAAgRgRgAgKgmQgGACgEAFQgEAFgDAIQgDAIAAAKQAAALADAIQACAIAFAEQAEAFAFADQAGACAFAAQAGAAAGgCQAFgDAEgFQAFgFACgIQADgIAAgKQAAgKgDgIQgDgIgEgFQgEgFgFgCQgGgCgGAAQgFAAgFACg");
	this.shape_4.setTransform(48.2,77.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AguAvQgRgRAAgeQAAgdARgRQARgRAdAAQAeAAARARQARARAAAdQAAAegRARQgRARgeAAQgdAAgRgRgAgKgmQgGACgEAFQgEAFgDAIQgDAIAAAKQAAALADAIQACAIAFAEQAEAFAFADQAGACAFAAQAGAAAGgCQAFgDAEgFQAFgFACgIQADgIAAgKQAAgKgDgIQgDgIgEgFQgEgFgFgCQgGgCgGAAQgFAAgFACg");
	this.shape_5.setTransform(28,77.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgPA+IAAhjIgnAAIAAgYIBsAAIAAAYIgmAAIAABjg");
	this.shape_6.setTransform(15,77.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AASA+IgkgyIgHAJIAAApIggAAIAAh7IAgAAIAAA4IArg4IAlAAIgwA5IAzBCg");
	this.shape_7.setTransform(-2.3,77.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgPA8QgLgEgJgIQgIgIgFgMQgEgMgBgQQABgOAEgMQAEgMAJgIQAIgIALgFQANgEAMAAIAPABIAMACIAJADIAJAEIAAAeIgFAAIgFgFIgIgFIgKgFQgFgBgGAAQgHAAgGACQgFACgFAFQgFAFgEAHQgCAIAAAKQgBAMAEAIQADAHAFAFQAGAEAEACQAHACAGAAIAMgBIAKgFIAIgFIAFgFIAEAAIAAAeIgJADIgJAEIgMACIgPABQgMAAgMgEg");
	this.shape_8.setTransform(-15.8,77.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgjA+IAAgWIAUAAIAAhPIgUAAIAAgWIBHAAIAAAWIgUAAIAABPIAUAAIAAAWg");
	this.shape_9.setTransform(-26.6,77.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgrA+IAAh7IAfAAIAABjIA4AAIAAAYg");
	this.shape_10.setTransform(-36,77.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgPA8QgLgEgJgIQgIgIgFgMQgFgMABgQQgBgOAFgMQAFgMAHgIQAJgIAMgFQAMgEANAAIAOABIALACIALADIAHAEIAAAeIgEAAIgFgFIgIgFIgKgFQgGgBgGAAQgGAAgGACQgFACgGAFQgEAFgDAHQgEAIAAAKQABAMADAIQADAHAFAFQAFAEAGACQAFACAHAAIALgBIALgFIAHgFIAGgFIADAAIAAAeIgJADIgJAEIgLACIgOABQgNAAgMgEg");
	this.shape_11.setTransform(-48.1,77.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(43));

	// hand
	this.instance = new lib.Tween5("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(31.2,63.5);
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:17.8,y:21.3,alpha:1},10).to({regX:0.1,regY:21.4,scaleY:0.82,rotation:1,x:17.9,y:42.7},1).to({regX:0,regY:0,scaleY:1,rotation:0,x:17.8,y:21.3},1).wait(31));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-183,42.2,405.5,46.8);


// stage content:
(lib.platsaInteriorotvaranje11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		this.BigLeft.visible=false;
		this.BigRight.visible=false;
		this.SmallLeft.visible=false;
		this.SmallRight.visible=false;
		this.MediumLeft.visible=false;
		this.MediumRight.visible=false;
	}
	this.frame_1 = function() {
		this.ButtonBigLeft.addEventListener("click", fl_BigLeft.bind(this));
		this.ButtonBigRight.addEventListener("click", fl_BigRight.bind(this));
		this.ButtonSmallRight.addEventListener("click", fl_SmallRight.bind(this));
		this.ButtonSmallLeft.addEventListener("click", fl_SmallLeft.bind(this));
		this.ButtonMediumRight.addEventListener("click", fl_MediumRight.bind(this));
		this.ButtonMediumLeft.addEventListener("click", fl_MediumLeft.bind(this));
		
		function fl_BigLeft()
		{
		if (this.BigLeft.visible==false) 
			{
			this.BigLeft.visible=true;
			console.log("Otvoreno") 
			this.hand.visible=false;
			this.hotspotBigLeft.visible=false;
			}
		else 
			{
			this.BigLeft.visible=false;
			console.log("Zatvoreno")
			this.hand.visible=false;
			this.hotspotBigLeft.visible=true;
			}
		}
		
		function fl_BigRight()
		{
		if (this.BigRight.visible==false) 
			{
			this.BigRight.visible=true;
			this.hand.visible=false;		
			console.log("Otvoreno") 
			this.hotspotBigRight.visible=false;
			}
		else 
			{
			this.BigRight.visible=false;
			this.hand.visible=false;		
			console.log("Zatvoreno") 	
			this.hotspotBigRight.visible=true;
			}
		}
		
		function fl_MediumRight()
		{
		if (this.MediumRight.visible==false) 
			{
			this.MediumRight.visible=true;
			this.hand.visible=false;	
			this.hotspotMediumRight.visible=false;	
			console.log("Otvoreno") 
			}
		else 
			{
			this.MediumRight.visible=false;
			this.hotspotMediumRight.visible=true;		
			this.hand.visible=false;		
			console.log("Zatvoreno") 	
			}
		}
		
		function fl_MediumLeft()
		{
		if (this.MediumLeft.visible==false) 
			{
			this.MediumLeft.visible=true;
			this.hotspotMediumLeft.visible=false;		
			this.hand.visible=false;		
			console.log("Otvoreno") 
			}
		else 
			{
			this.MediumLeft.visible=false;
			this.hand.visible=false;	
			this.hotspotMediumLeft.visible=true;		
			console.log("Zatvoreno") 	
			}
		}
		
		function fl_SmallRight()
		{
		if (this.SmallRight.visible==false) 
			{
			this.SmallRight.visible=true;
			this.hotspotSmallRight.visible=false;		
			this.hand.visible=false;		
			console.log("Otvoreno") 
			}
		else 
			{
			this.SmallRight.visible=false;
			this.hand.visible=false;	
			this.hotspotSmallRight.visible=true;	
			console.log("Zatvoreno") 	
			}
		}
		
		function fl_SmallLeft()
		{
		if (this.SmallLeft.visible==false) 
			{
			this.SmallLeft.visible=true;
			this.hotspotSmallLeft.visible=false;	
			this.hand.visible=false;		
			console.log("Otvoreno") 
			}
		else 
			{
			this.SmallLeft.visible=false;
			this.hand.visible=false;	
			this.hotspotSmallLeft.visible=true;		
			console.log("Zatvoreno") 	
			}
		}
		
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1).call(this.frame_1).wait(1));

	// hotspots
	this.hotspotBigLeft = new lib.hotspot();
	this.hotspotBigLeft.name = "hotspotBigLeft";
	this.hotspotBigLeft.parent = this;
	this.hotspotBigLeft.setTransform(803.2,347.2,1,1,0,0,0,5.8,5.8);

	this.hotspotSmallLeft = new lib.hotspotcopy3();
	this.hotspotSmallLeft.name = "hotspotSmallLeft";
	this.hotspotSmallLeft.parent = this;
	this.hotspotSmallLeft.setTransform(803.2,160.4,1,1,0,0,0,5.8,5.8);

	this.hotspotBigRight = new lib.hotspotcopy4();
	this.hotspotBigRight.name = "hotspotBigRight";
	this.hotspotBigRight.parent = this;
	this.hotspotBigRight.setTransform(900.6,347.2,1,1,0,0,0,5.8,5.8);

	this.hotspotMediumRight = new lib.hotspot();
	this.hotspotMediumRight.name = "hotspotMediumRight";
	this.hotspotMediumRight.parent = this;
	this.hotspotMediumRight.setTransform(1544.4,795.9,1,1,0,0,0,5.8,5.8);

	this.hotspotMediumLeft = new lib.hotspotcopy();
	this.hotspotMediumLeft.name = "hotspotMediumLeft";
	this.hotspotMediumLeft.parent = this;
	this.hotspotMediumLeft.setTransform(1136.5,814.2,1,1,0,0,0,5.8,5.8);

	this.hotspotSmallRight = new lib.hotspotcopy5();
	this.hotspotSmallRight.name = "hotspotSmallRight";
	this.hotspotSmallRight.parent = this;
	this.hotspotSmallRight.setTransform(1299,640,1,1,0,0,0,5.8,5.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.hotspotSmallRight},{t:this.hotspotMediumLeft},{t:this.hotspotMediumRight},{t:this.hotspotBigRight},{t:this.hotspotSmallLeft},{t:this.hotspotBigLeft}]}).wait(2));

	// hand
	this.hand = new lib.hand();
	this.hand.name = "hand";
	this.hand.parent = this;
	this.hand.setTransform(725.1,398.7,1.116,1.116,0,0,0,17.9,21.4);
	this.hand._off = true;

	this.timeline.addTween(cjs.Tween.get(this.hand).wait(1).to({_off:false},0).wait(1));

	// Buttons Open
	this.ButtonBigLeft = new lib.ButtonBigLeft();
	this.ButtonBigLeft.name = "ButtonBigLeft";
	this.ButtonBigLeft.parent = this;
	this.ButtonBigLeft.setTransform(724.2,648.2,1,1,0,0,0,126,374.1);
	this.ButtonBigLeft.alpha = 0.012;
	new cjs.ButtonHelper(this.ButtonBigLeft, 0, 1, 1);

	this.ButtonSmallLeft = new lib.ButtonBigLeft();
	this.ButtonSmallLeft.name = "ButtonSmallLeft";
	this.ButtonSmallLeft.parent = this;
	this.ButtonSmallLeft.setTransform(723.4,188.1,0.992,0.23,0,0,0,126.2,373.9);
	this.ButtonSmallLeft.alpha = 0.012;
	new cjs.ButtonHelper(this.ButtonSmallLeft, 0, 1, 1);

	this.ButtonSmallRight = new lib.ButtonBigLeft();
	this.ButtonSmallRight.name = "ButtonSmallRight";
	this.ButtonSmallRight.parent = this;
	this.ButtonSmallRight.setTransform(1224.4,678.2,0.968,0.23,0,0,0,126.1,373.9);
	this.ButtonSmallRight.alpha = 0.012;
	new cjs.ButtonHelper(this.ButtonSmallRight, 0, 1, 1);

	this.ButtonMediumRight = new lib.ButtonBigLeft();
	this.ButtonMediumRight.name = "ButtonMediumRight";
	this.ButtonMediumRight.parent = this;
	this.ButtonMediumRight.setTransform(1468.4,888.3,0.968,0.358,0,0,0,126,374.2);
	this.ButtonMediumRight.alpha = 0.012;
	new cjs.ButtonHelper(this.ButtonMediumRight, 0, 1, 1);

	this.ButtonMediumLeft = new lib.ButtonBigLeft();
	this.ButtonMediumLeft.name = "ButtonMediumLeft";
	this.ButtonMediumLeft.parent = this;
	this.ButtonMediumLeft.setTransform(1224.3,893.2,0.968,0.345,0,0,0,126,374);
	this.ButtonMediumLeft.alpha = 0.012;
	new cjs.ButtonHelper(this.ButtonMediumLeft, 0, 1, 1);

	this.ButtonBigRight = new lib.ButtonBigLeft();
	this.ButtonBigRight.name = "ButtonBigRight";
	this.ButtonBigRight.parent = this;
	this.ButtonBigRight.setTransform(976.3,648.2,1,1,0,0,0,126,374.1);
	this.ButtonBigRight.alpha = 0.012;
	new cjs.ButtonHelper(this.ButtonBigRight, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.ButtonBigRight},{t:this.ButtonMediumLeft},{t:this.ButtonMediumRight},{t:this.ButtonSmallRight},{t:this.ButtonSmallLeft},{t:this.ButtonBigLeft}]}).wait(2));

	// Small right
	this.SmallRight = new lib.Layer4_1();
	this.SmallRight.name = "SmallRight";
	this.SmallRight.parent = this;
	this.SmallRight.setTransform(1218.5,691.5,1,1,0,0,0,122.5,98.5);

	this.timeline.addTween(cjs.Tween.get(this.SmallRight).wait(2));

	// Medium Right
	this.MediumRight = new lib.Layer6_1();
	this.MediumRight.name = "MediumRight";
	this.MediumRight.parent = this;
	this.MediumRight.setTransform(1469.5,904.5,1,1,0,0,0,128.5,146.5);

	this.timeline.addTween(cjs.Tween.get(this.MediumRight).wait(2));

	// Medium Left
	this.MediumLeft = new lib.Layer5_1();
	this.MediumLeft.name = "MediumLeft";
	this.MediumLeft.parent = this;
	this.MediumLeft.setTransform(1221.5,914.5,1,1,0,0,0,122.5,151.5);

	this.timeline.addTween(cjs.Tween.get(this.MediumLeft).wait(2));

	// Big Right
	this.BigRight = new lib.Layer3_1();
	this.BigRight.name = "BigRight";
	this.BigRight.parent = this;
	this.BigRight.setTransform(972,656.5,1,1,0,0,0,126,410.5);

	this.timeline.addTween(cjs.Tween.get(this.BigRight).wait(2));

	// mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EgBBBC2QALh9AJhVQADgTAOimQAMiQAMhMQALhRAfhDQAihAANgcQAQglgbgyQgWgogmgcQgbgVglgLQgggJg1gEQgsgDhJAGQg0AEhpANQhMAKiTgCIiFgDMgZ2h/6MBLnAMNMgD1CNRMghEAA3g");
	mask.setTransform(664.2,592.9);

	// Big Left
	this.BigLeft = new lib.Layer2_1();
	this.BigLeft.name = "BigLeft";
	this.BigLeft.parent = this;
	this.BigLeft.setTransform(726.1,633,1,1,0,0,0,126.5,389);

	var maskedShapeInstanceList = [this.BigLeft];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.BigLeft).wait(2));

	// Small left
	this.SmallLeft = new lib.Layer1_1();
	this.SmallLeft.name = "SmallLeft";
	this.SmallLeft.parent = this;
	this.SmallLeft.setTransform(724,168.5,1,1,0,0,0,125,109.5);

	this.timeline.addTween(cjs.Tween.get(this.SmallLeft).wait(2));

	// ZATVORENO
	this.instance = new lib.ZATVORENO_1();
	this.instance.parent = this;
	this.instance.setTransform(960,640,1,1,0,0,0,960,640);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(960,640,1920,1280);
// library properties:
lib.properties = {
	id: 'F2CFE711181443C6BAEEECFCFE8F1089',
	width: 1920,
	height: 1280,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/ZATVORENO.jpg", id:"ZATVORENO"},
		{src:"images/platsa Interior otvaranje 1.1_atlas_P_.png", id:"platsa Interior otvaranje 1.1_atlas_P_"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['F2CFE711181443C6BAEEECFCFE8F1089'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;