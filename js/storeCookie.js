function setCookie(cname,cvalue,exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function cookieSettings(){
    $("#cookiePolicy").html('<div class="cookieBox"><div class="cookieMainLeft"><h3 class="cookieHeadline">Tvoje postavke kolačića</h3><div class="cookieSection"><input id="check-disabled" type="checkbox" checked class="inputCookiea"><p style=""><strong style="color: #FF671D">Funkcijski kolačići</strong><br/>Ovi su kolačići potrebni radi osnovnih funkcionalnosti internetske stranice i samim time su uvijek omogućeni. To uključuje kolačiće koji omogućavaju spremanje određenih postavki sukladno tvojim interesima, dok ti pretražuješ Internet.</p></div><div class="cookieSection"><input id="preformanceCookie" type="checkbox" class="inputCookiea"><p style=""><strong>Kolačići učinkovitosti</strong><br/>Ovi nam kolačići omogućavaju unaprjeđenje radnih svojstava internetske stranice praćenjem njezine korištenosti. Također nam pomaže u unaprjeđenju iskustva pretraživanja internetske stranice koju planiramo izraditi u budućnosti.</p></div><div class="cookieSection"><input id="socialCookie" type="checkbox" class="inputCookie"><p style=""><strong>Kolačići korišteni za društvene mreže i oglašavanje</strong><br/>Kolačići korišteni za društvene mreže nude ti mogućnost povezivanja s društvenim mrežama, kao i dijeljenje sadržaja s tvoje internetske stranice putem njih. Kolačići korišteni za oglašavanje (od treće strane) prikupljaju informacije kako bi prikazali oglase sukladno tvojim interesima, kako s IKEA internetske stranice, tako i s drugih internetskih stranica. U nekim slučajevima ovi kolačići uključuju obradu tvojih osobnih podataka.<br/>Za više informacija o obradi tvojih osobnih podataka, pogledaj naša Pravila o zaštiti osobnih podataka i Pravila o kolačićima. Ne prihvaćanje korištenja kolačića može rezultirati prikazom oglasa koji nisu u skladu s tvojim interesima, kao i nemogućnosti učinkovitog povezivanja s Facebookom, Twitterom ili drugim društvenim mrežama i/ili nemogućnosti dijeljenja sadržaja na društvenim mrežama.</p></div><div class="cookieSectionBot"><p>Svoje odabrane postavke uvijek možeš promijeniti odlaskom na „Postavke kolačića" koje se nalaze na dnu naše internetske stranice. Za više detalja pogledaj naša Pravila o zaštiti osobnih podataka i pravila o kolačićima.</p></div></div><div class="cookieMainRight"><img src="images/cookies.png" style="width: 100%;"><button class="doneButton" onclick="checkBoxStatus()">SPREMI</button></div></div>');
    document.getElementById("check-disabled").disabled = true;
    $('#socialCookie').prop('checked', true);
    $('#preformanceCookie').prop('checked', true);
    var preformance = getCookie("preformance");
    var social = getCookie("social");

    if(social == "YES"){
        $('#socialCookie').prop('checked', true);
    }

    if(preformance == "YES"){
        $('#preformanceCookie').prop('checked', true);
    }

    if(social == "NO"){
        $('#socialCookie').prop('checked', false);
    }

    if(preformance == "NO"){
        $('#preformanceCookie').prop('checked', false);
    }
};

function checkBoxStatus() {
    var social = $('#socialCookie').is(':checked');
    var analitycs = $('#preformanceCookie').is(':checked');
    var cookie = "YES";
    var mainCookie = "Accepted";

    if ( social == 1 ){
        setCookie("social", cookie, 365);
        setCookie("cookiePolicyA", mainCookie, 365);
    }
    if (analitycs == 1 ){
        setCookie("preformance", cookie, 365);
        setCookie("cookiePolicyA", mainCookie, 365);
    }
    if ( social == 0 ){
        setCookie("social", "NO", 365);
        setCookie("cookiePolicyA", mainCookie, 365);
    }
    if (analitycs == 0 ){
        setCookie("preformance", "NO", 365);
        setCookie("cookiePolicyA", mainCookie, 365);
    }
    else {
        setCookie("cookiePolicyA", mainCookie, 365);
    }

    location.reload();
}

function SetAllStatus() {
    var cookie = "YES";
    var mainCookie = "Accepted";
    setCookie("preformance", cookie, 365);
    setCookie("social", cookie, 365);
    setCookie("cookiePolicyA", mainCookie, 365);
    location.reload();
}


function addGA() {
    $("head").find(":last").after("<script id='gaScript' async src='https://www.googletagmanager.com/gtag/js id=UA-103274080-21'></script><script id='gaScript1'>window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-103274080-21');</script>");
}

function removeGA(){
    debugger;
    setCookie("_gid", "Deleted", -365);
    setCookie("_ga", "Deleted", -365);
    setCookie("_gat_gtag_UA_103274080_21", "Deleted", -365);
    $('#gaScript').remove();
    $('#gaScript1').remove();
}

function addSocial() {
    $('#socialAnalytics').html('<script type="text/javascript"> var google_conversion_id = 972447822; var google_custom_params = window.google_tag_params; var google_remarketing_only = true; </script><script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script><noscript><div style="display:inline;"><img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/972447822/?guid=ON&amp;script=0"/></div></noscript><script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version="2.0";n.queue=[];t=b.createElement(e);t.async=!0; t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,"script","https://connect.facebook.net/en_US/fbevents.js");fbq("init", "677350852350180"); fbq("track", "PageView");</script><noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=677350852350180&ev=PageView&noscript=1"/></noscript><img src="https://secure.adnxs.com/px?id=987729&seg=12435055&t=2" width="1" height="1" style="display:none"/>');
}

function removeSocial() {
    $('#socialAnalytics').empty();
}

function checkCookie() {
    var cookie = getCookie("cookiePolicyA");
    var preformance = getCookie("preformance");
    var social = getCookie("social");
    if (preformance == "YES" ) {
        addGA();
    }

    if (social == "YES" ) {
        addSocial();
    }

    if (cookie != "" && preformance == "NO") {
        removeGA();
    }

    if (cookie != "" && social == "NO") {
        removeSocial();
    }

    if (cookie == "") {
        $("#cookiePolicy").html('<div class="cookieBox"><div class="cookieAlert1"><img style="width: 100%" src="images/cookies.png"></div><div class="cookieAlert2"><h4>Tvoje postavke kolačića</h4><p>IKEA te traži suglasnost za korištenje kolačića u svrhu poboljšanja naše usluge, oglašavanja i korištenja društvenih mreža. Kolačići korišteni za oglašavanje i društvene mreže treće strane nude ti različiti izbor funkcija s društvenih mreža i oglasa sukladno tvojim osobnim interesima. Za više informacija ili unaprjeđenje tvojih postavki, odaberi gumb „Više“ ili "Postavke kolačića" koji se nalazi na dnu naše internetske stranice. <br/> Za više informacija o kolačićima i korištenju tvojih osobnih podataka, pročitaj naša Pravila o zaštiti podataka i Pravila o kolačićima. Prihvaćaš li ove kolačiće i pristaješ li na korištenje tvojih osobnih podataka?</p><a target="_blank" href="https://www.ikea.com/ms/hr_HR/privacy_policy/privacy_policy.html">Zaštita privatnosti</a><span class="separator"> | </span><a target="_blank" href="https://www.ikea.com/ms/hr_HR/privacy_policy/cookie_policy.html">Pravila o kolačićima</a></div><div class="cookieAlert3"><button onclick="SetAllStatus()">PRIHVAĆAM</button><button class="moreInfo" onclick="cookieSettings()">VIŠE</button></div></div>')
    }
}
