var canvas1, stage1, exportRoot1, anim_container1, dom_overlay_container1, fnStartAnimation1;
var animWidth1 = $('.plannerAnimationHolder').width();
function init1() {
	canvas1 = document.getElementById("canvas1");
	anim_container1 = document.getElementById("animation_container1");
	dom_overlay_container1 = document.getElementById("dom_overlay_container1");
	var comp=AdobeAn.getComposition("0D82343838F341E78C4106A24DE1B948");
	var lib=comp.getLibrary();
	var loader = new createjs.LoadQueue(false);
	loader.addEventListener("fileload", function(evt){handleFileLoad1(evt,comp)});
	loader.addEventListener("complete", function(evt){handleComplete1(evt,comp)});
	var lib=comp.getLibrary();
	loader.loadManifest(lib.properties.manifest);
}
function handleFileLoad1(evt, comp) {
	var images=comp.getImages();
	if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
}
function handleComplete1(evt,comp) {
	//This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
	var lib=comp.getLibrary();
	var ss=comp.getSpriteSheet();
	var queue = evt.target;
	var ssMetadata = lib.ssMetadata;
	for(i=0; i<ssMetadata.length; i++) {
		ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
	}
	exportRoot1 = new lib.planneranimation();
	stage1 = new lib.Stage(canvas1);
	//Registers the "tick" event listener.
	fnStartAnimation1 = function() {
		stage1.addChild(exportRoot1);
		createjs.Ticker.setFPS(lib.properties.fps);
		createjs.Ticker.addEventListener("tick", stage1);
	}
	//Code to support hidpi screens and responsive scaling.
	function makeResponsive(isResp, respDim, isScale, scaleType) {
		var lastW, lastH, lastS=1;
		window.addEventListener('resize', resizeCanvas1);
		resizeCanvas1();
		function resizeCanvas1() {
			//var w = lib.properties.width, h = lib.properties.height;
			//var iw = window.innerWidth, ih=window.innerHeight;
			var w = lib.properties.width, h = lib.properties.height;
			var iw = animWidth1, ih= animWidth1 * 0.48;
			var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h,
			sRatio=1;
			if(isResp) {
				if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
					sRatio = lastS;
				}
				else if(!isScale) {
					if(iw<w || ih<h)
						sRatio = Math.min(xRatio, yRatio);
				}
				else if(scaleType==1) {
					sRatio = Math.min(xRatio, yRatio);
				}
				else if(scaleType==2) {
					sRatio = Math.max(xRatio, yRatio);
				}
			}
			canvas1.width = w*pRatio*sRatio;
			canvas1.height = h*pRatio*sRatio;
			canvas1.style.width = dom_overlay_container1.style.width = anim_container1.style.width =  w*sRatio+'px';
			canvas1.style.height = anim_container1.style.height = dom_overlay_container1.style.height = h*sRatio+'px';
			stage1.scaleX = pRatio*sRatio;
			stage1.scaleY = pRatio*sRatio;
			lastW = iw; lastH = ih; lastS = sRatio;
			stage1.tickOnUpdate = false;
			stage1.update();
			stage1.tickOnUpdate = true;
		}
	}
	makeResponsive(true,'both',false,1);
	AdobeAn.compositionLoaded(lib.properties.id);
	fnStartAnimation1();
}